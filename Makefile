# Copyright (C) 2015-2023 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

LIBNAME_STATIC = libstar-sp.a
LIBNAME_SHARED = libstar-sp.so
LIBNAME = $(LIBNAME_$(LIB_TYPE))

################################################################################
# Star-SP building
################################################################################
SRC =\
 src/ssp_ran.c\
 src/ssp_ranst_discrete.c\
 src/ssp_ranst_gaussian.c\
 src/ssp_ranst_piecewise_linear.c\
 src/ssp_rng.c\
 src/ssp_rng_proxy.c

OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

build_library: .config $(DEP)
	@$(MAKE) -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) \
	$$(if [ -n "$(LIBNAME)" ]; then \
	     echo "$(LIBNAME)"; \
	   else \
	     echo "$(LIBNAME_SHARED)"; \
	   fi)

$(DEP) $(OBJ): config.mk

$(LIBNAME_SHARED): $(OBJ)
	$(CXX) $(CXXFLAGS) $(AES_CFLAGS) $(DPDC_CFLAGS) -o $@ $(OBJ) $(LDFLAGS_SO) $(DPDC_LIBS)

$(LIBNAME_STATIC): libstar-sp.o
	$(AR) -rc $@ $?
	$(RANLIB) $@

libstar-sp.o: $(OBJ)
	$(LD) -r $(OBJ) -o $@
	$(OBJCOPY) $(OCPFLAGS) $@

.config: config.mk
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then \
	   echo "rsys $(RSYS_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(RANDOM123_VERSION) random123; then \
	   echo "random123 $(RANDOM123_VERSION) not found" >&2; exit 1; fi
	@echo "config done" > .config

.SUFFIXES: .c .d .o
.c.d:
	@$(CXX) $(CXXFLAGS) $(DPDC_CFLAGS) $(AES_CFLAGS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CXX) $(CXXFLAGS) $(DPDC_CFLAGS) $(AES_CFLAGS) -DSSP_SHARED_BUILD -c $< -o $@

################################################################################
# Installation
################################################################################
pkg:
	sed -e 's#@PREFIX@#$(PREFIX)#g' \
	    -e 's#@VERSION@#$(VERSION)#g' \
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g' \
	    -e 's#@RANDOM123_VERSION@#$(RANDOM123_VERSION)#g' \
	    star-sp.pc.in > star-sp.pc

star-sp-local.pc: config.mk star-sp.pc.in
	sed -e '1d'\
	    -e 's#^includedir=.*#includedir=./src/#'\
	    -e 's#^libdir=.*#libdir=./#'\
	    -e 's#@PREFIX@#$(PREFIX)#g'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    -e 's#@RANDOM123_VERSION@#$(RANDOM123_VERSION)#g'\
	    star-sp.pc.in > $@

install: build_library pkg
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib" $(LIBNAME)
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib/pkgconfig" star-sp.pc
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/star" src/ssp.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/star-sp" COPYING README.md

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/lib/libstar-sp.so
	rm -f $(DESTDIR)$(PREFIX)/lib/pkgconfig/star-sp.pc
	rm -f $(DESTDIR)$(PREFIX)/share/doc/star-sp/COPYING
	rm -f $(DESTDIR)$(PREFIX)/share/doc/star-sp/README.md
	rm -f $(DESTDIR)$(PREFIX)/include/star/ssp.h

################################################################################
# Miscellaneous targets
################################################################################
all: build_library build_tests

clean: clean_test
	rm -f $(OBJ) $(TEST_OBJ) $(LIBNAME)
	rm -f .config .test libstar-sp.o star-sp.pc star-sp-local.pc

distclean: clean
	rm -f $(DEP) $(TEST_DEP)

lint:
	shellcheck -o all make.sh

################################################################################
# Tests
################################################################################
TEST_SRC =\
 src/test_ssp_ran_circle.c\
 src/test_ssp_ran_discrete.c\
 src/test_ssp_ran_gaussian.c\
 src/test_ssp_ran_hemisphere.c\
 src/test_ssp_ran_hg.c\
 src/test_ssp_ran_piecewise_linear.c\
 src/test_ssp_ran_sphere.c\
 src/test_ssp_ran_sphere_cap.c\
 src/test_ssp_ran_spherical_zone.c\
 src/test_ssp_ran_tetrahedron.c\
 src/test_ssp_ran_triangle.c\
 src/test_ssp_ran_uniform_disk.c\
 src/test_ssp_rng.c\
 src/test_ssp_rng_proxy.c
TEST_OBJ = $(TEST_SRC:.c=.o)
TEST_DEP = $(TEST_SRC:.c=.d)

PKG_CONFIG_LOCAL = PKG_CONFIG_PATH="./:$${PKG_CONFIG_PATH}" $(PKG_CONFIG)
SSP_CFLAGS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --cflags star-sp-local.pc)
SSP_LIBS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --libs star-sp-local.pc)

build_tests: build_library $(TEST_DEP) .test
	@$(MAKE) -fMakefile -f.test $$(for i in $(TEST_DEP); do echo -f"$${i}"; done) test_bin

test: build_tests
	@$(SHELL) make.sh run_test "$(AES_CFLAGS)" $(TEST_SRC)

.test: Makefile
	@$(SHELL) make.sh config_test $(TEST_SRC) > .test

clean_test:
	@$(SHELL) make.sh clean_test $(TEST_SRC)

$(TEST_DEP): config.mk star-sp-local.pc
	@$(CC) $(CFLAGS) $(SSP_CFLAGS) $(RSYS_CFLAGS) -MM -MT "$(@:.d=.o) $@" $(@:.d=.c) -MF $@

$(TEST_OBJ): config.mk star-sp-local.pc
	$(CC) $(CFLAGS) $(SSP_CFLAGS) $(RSYS_CFLAGS) -c $(@:.o=.c) -o $@

test_ssp_ran_circle \
test_ssp_ran_discrete \
test_ssp_ran_gaussian \
test_ssp_ran_hemisphere \
test_ssp_ran_hg \
test_ssp_ran_piecewise_linear \
test_ssp_ran_sphere \
test_ssp_ran_sphere_cap \
test_ssp_ran_spherical_zone \
test_ssp_ran_tetrahedron \
test_ssp_ran_triangle \
test_ssp_ran_uniform_disk \
test_ssp_rng \
test_ssp_rng_proxy \
: config.mk star-sp-local.pc $(LIBNAME)
	$(CC) $(CFLAGS) -o $@ src/$@.o $(LDFLAGS_EXE) $(SSP_LIBS) $(RSYS_LIBS) -lm

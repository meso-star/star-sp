#!/bin/sh

# Copyright (C) 2015-2023 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

config_test()
{
  for i in "$@"; do
    test=$(basename "${i}" ".c")
    test_list="${test_list} ${test}"
    printf "%s: %s\n" "${test}" "src/${test}.o"
  done
  printf "test_bin: %s\n" "${test_list}"
}

check()
{
  name="$1"
  prog="$2"
  shift 2

  printf "%s " "${name}"
  if ./"${prog}" "$@" > /dev/null 2>&1; then
    printf "\033[1;32mOK\033[m\n"
  else
    printf "\033[1;31mError\033[m\n"
  fi
}

run_test()
{
  aes_flags=$1
  shift 1

  for i in "$@"; do
    test=$(basename "${i}" ".c")
    if ! [ "${test}" = "test_ssp_rng" ]; then
      check "${test}" "${test}"
    else
      if [ -n "${aes_flags}" ]; then
        check "${test}_aes" "${test}" aes
      fi
      check "${test}_kiss" "${test}" kiss
      check "${test}_mt19937_64" "${test}" mt19937_64
      check "${test}_ranlux48" "${test}" ranlux48
      check "${test}_random_device" "${test}" random_device
      check "${test}_threefry" "${test}" threefry
    fi
  done 2> /dev/null
}

clean_test()
{
  for i in "$@"; do
    rm -f "$(basename "${i}" ".c")"
  done
}

install()
{
  prefix=$1
  shift 1

  mkdir -p "${prefix}"

  for i in "$@"; do
    dst="${prefix}/${i##*/}"

    if cmp -s "${i}" "${dst}"; then
      printf "Up to date %s\n" "${dst}"
    else
      printf "Installing %s\n" "${dst}"
      cp "${i}" "${prefix}"
    fi
  done
}

"$@"

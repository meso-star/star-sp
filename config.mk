VERSION = 0.14.0
PREFIX = /usr/local

LIB_TYPE = SHARED
#LIB_TYPE = STATIC

BUILD_TYPE = RELEASE
#BUILD_TYPE = DEBUG

################################################################################
# Tools
################################################################################
AR = ar
CC = cc
CXX = c++
LD = ld
OBJCOPY = objcopy
PKG_CONFIG = pkg-config
RANLIB = ranlib

################################################################################
# Dependencies
################################################################################
PCFLAGS_SHARED =
PCFLAGS_STATIC = --static
PCFLAGS = $(PCFLAGS_$(LIB_TYPE))

RSYS_VERSION = 0.14
RSYS_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags rsys)
RSYS_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs rsys)

RANDOM123_VERSION = 1.14
RANDOM123_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags random123)
RANDOM123_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs random123)

DPDC_CFLAGS = $(RSYS_CFLAGS) $(RANDOM123_CFLAGS)
DPDC_LIBS = $(RSYS_LIBS) $(RANDOM123_LIBS)

################################################################################
# Compilation options
################################################################################
# Comment to disable the Random123 AES RNG
AES_CFLAGS = -DWITH_R123_AES -maes

FLAGS =\
 -pedantic\
 -fvisibility=hidden\
 -fstrict-aliasing\
 -Wall\
 -Wconversion\
 -Wextra\
 -Wmissing-declarations\
 -Wshadow

CFLAGS_HARDENED =\
 -D_FORTIFY_SOURCES=2\
 -fcf-protection=full\
 -fstack-clash-protection\
 -fstack-protector-strong

CFLAGS_COMMON =\
 -std=c89\
 -Wmissing-prototypes\
 $(CFLAGS_HARDENED)\
 $(FLAGS)

CXXFLAGS_COMMON =\
 -std=c++11\
 $(CFLAGS_HARDENED)\
 $(FLAGS)

CFLAGS_DEBUG = -g $(CFLAGS_COMMON)
CFLAGS_RELEASE = -O2 -DNDEBUG $(CFLAGS_COMMON)
CFLAGS = $(CFLAGS_$(BUILD_TYPE)) -fPIE

CXXFLAGS_RELEASE = -O2 -DNDEBUG $(CXXFLAGS_COMMON)
CXXFLAGS_DEBUG = -g $(CXXFLAGS_COMMON)
CXXFLAGS = $(CXXFLAGS_$(BUILD_TYPE)) -fPIC

################################################################################
# Linker options
################################################################################
LDFLAGS_HARDENED = -Wl,-z,relro,-z,now
LDFLAGS_DEBUG = $(LDFLAGS_HARDENED)
LDFLAGS_RELEASE = -s $(LDFLAGS_HARDENED)
LDFLAGS = $(LDFLAGS_$(BUILD_TYPE))

LDFLAGS_SO = $(LDFLAGS) -shared -Wl,--no-undefined
LDFLAGS_EXE = $(LDFLAGS) -pie

OCPFLAGS_DEBUG = --localize-hidden
OCPFLAGS_RELEASE = --localize-hidden --strip-unneeded
OCPFLAGS = $(OCPFLAGS_$(BUILD_TYPE))
